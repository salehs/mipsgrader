"""
Database.py - Database class of MIPS Grader Application
This class is responsible of executing SQL queries to sqlite database.
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
import sqlite3

class Database(object):
  """
  Database class
  """
  __DB_LOCATION = "mipsGraderdb.db"
  def __init__(self):
    """Initialize db class variables"""
    self.connection = sqlite3.connect(Database.__DB_LOCATION, check_same_thread=False)
    self.cur = self.connection.cursor()

  def close(self):
    """close sqlite3 connection"""
    self.connection.close()

  def execute(self, command):
    """execute an sql command in the current cursor"""
    self.cur.execute(command)
    return self.cur.fetchall()

  def commit(self):
    """commit changes to database"""
    self.connection.commit()
