"""
GraderQWorker.py - Grader QWorker class of MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
import os
import sys
from subprocess import PIPE, run, CalledProcessError, TimeoutExpired
import shutil
from difflib import SequenceMatcher
from PyQt5.QtCore import QObject, pyqtSignal
from PyQt5.QtWidgets import QMessageBox
from models.Task import Task
from models.Student import Student
from models.TestCase import TestCase
from models.Lab_Grade import Lab_Grade

class GraderQWorker(QObject):
  finished = pyqtSignal()
  _workDoneSignal = pyqtSignal(int)
  def __init__(self, parent, db, labid, sectionid, folderPath):
    super().__init__()
    self.parent = parent
    self.db = db
    self.labid = labid
    self.sectionid = sectionid
    self.folderPath = folderPath
  def run(self):
    """ Grading Lab Tasks """
    tasks = Task.getTasksOfLab(self.db, self.labid)
    java_path = self.locate_java()
    if java_path is None:
      QMessageBox.information(self.parent, "Critical", "Could not find java on this system", buttons=QMessageBox.Ok)
      return
    students = Student.getStudentsOfSection(self.db, self.sectionid)
    students_id = []
    
    for student in students:
      students_id.append(student.studentid)
    self.organizeFolder(self.folderPath, students_id)
    test_cases_run = 0
    totalWork = 0
    for task in tasks: 
      test_cases = TestCase.getTestCasesOfTask(self.db, task.taskid)
      totalWork += len(students) *  len(test_cases) 
    for student in students:
      sid = str(student.studentid)
      filesOnly = [os.path.join(self.folderPath, sid, f).replace("\\", "/") for f in os.listdir(os.path.join(self.folderPath, sid)) if os.path.isfile(os.path.join(self.folderPath, sid, f)) and not f.endswith(".txt")]
      lab_grade = 0.0
      student_files = [file_st for file_st in filesOnly if sid in file_st]
      for task in tasks:
        task_grade = 0.0
        task_files = [f for f in student_files if task.name.lower() in f.lower()]
        passed_test_cases = 0.0
        if task_files:
          task_file = task_files[0]
          test_cases = TestCase.getTestCasesOfTask(self.db, task.taskid)
          
          for test_case in test_cases:
            assembly_args = test_case.arguments.replace(",", "\n")
            command = [java_path, "-jar", "Mars4_5.jar", "nc", task_file]
            actual_output = ""
            try:
              if os.name == "nt":
                process = run(command, encoding="utf-8", input=assembly_args, stdout=PIPE, shell=True, timeout=120, check=True)
                actual_output = process.stdout.replace("\n", "")
              elif sys.platform.startswith("linux"):
                process = run(command, capture_output=True, input=assembly_args.encode(), timeout=120, check=True)
                actual_output = process.stdout.decode().replace("\n", "")
              
            except TimeoutExpired as e:
              print(e)
            except CalledProcessError as e:
              print(e)
            expected_output = test_case.expected_output.replace("\n", "")
            similarity = SequenceMatcher(None, actual_output, expected_output).ratio()
            # print("Actual Output:", actual_output)
            # print("Expected Output:", expected_output)
            # print("similarity:", similarity)
            if similarity >= 0.90:
              passed_test_cases += 1.0
            elif similarity >= 0.55:
              passed_test_cases += similarity
            test_cases_run += 1
            work_done = (test_cases_run * 100) // totalWork
            self._workDoneSignal.emit(work_done)
          task_grade = (passed_test_cases / len(test_cases)) * task.weight
          lab_grade += task_grade
      lab_grade = round(lab_grade, 2)
      labGrade = Lab_Grade(studentid=student.studentid, labid=self.labid, grade=lab_grade)
      labGrade.save(self.db)
    self.finished.emit()

  def organizeFolder(self, folderPath, students_id):
    filesOnly = [f for f in os.listdir(folderPath) if os.path.isfile(os.path.join(folderPath, f))]
    for sid in students_id:
      sid = str(sid)
      student_files = [file_st for file_st in filesOnly if sid in file_st]
      os.makedirs(os.path.join(folderPath, sid), exist_ok=True)
      for sfile in student_files:
        shutil.move(os.path.join(folderPath, sfile), os.path.join(folderPath, sid, sfile))

  def locate_java(self):
    if os.name == "nt":
        # Windows
      java_path = ""
      if 'JAVA_HOME' in os.environ:
        java_path = os.path.join(os.environ['JAVA_HOME'], "bin", "java.exe")
      elif os.path.exists("C:\\Program Files\\Java"):
        java_folder_dirs = os.listdir("C:\\Program Files\\Java")
        for java_dir in java_folder_dirs:
          if "jdk" in java_dir or "jre" in java_dir:
            java_path = os.path.join("C:\\Program Files\\Java", java_dir, "bin", "java.exe")
      elif os.path.exists("C:\\Program Files (x86)\\Java"):
        java_folder_dirs = os.listdir("C:\\Program Files (x86)\\Java")
        for java_dir in java_folder_dirs:
          if "jdk" in java_dir or "jre" in java_dir:
            java_path = os.path.join("C:\\Program Files (x86)\\Java", java_dir, "bin", "java.exe")
      if os.path.exists(java_path):
        return java_path
      else:
        return None
    elif sys.platform.startswith("linux"):
      # Linux
      process = run("which java", encoding="utf-8", input="", stdout=PIPE, shell=True, timeout=120, check=True)
      java_output = process.stdout.replace("\n", "")
#      print(java_output)
      if java_output != "":
        return java_output
      return None
    elif sys.platform == "darwin":
      # macOS
      # Todo: find java path on macOS machines
      return None
