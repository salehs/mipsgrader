"""
MainWindow.py - Main Window class of MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
import csv
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from Database import Database
from forms.SectionForm import SectionForm
from forms.LabForm import LabForm
from forms.StudentForm import StudentForm
from forms.TaskForm import TaskForm
from forms.TestCaseForm import TestCaseForm
from forms.CopyLabForm import CopyLabForm
from models.Section import Section
from models.Lab import Lab
from models.Student import Student
from models.Task import Task
from models.TestCase import TestCase
from models.Lab_Grade import Lab_Grade
from GraderQWorker import GraderQWorker

class MainWindow(QMainWindow):
  def __init__(self):
    super().__init__()
    self.setWindowTitle('MIPS Grader Application By Saleh AlSaleh')
    self.appIcon = QIcon('icon.bmp')
    self.setWindowIcon(self.appIcon)
    self.width = 1280
    self.height = 720
    self.resize(self.width, self.height)
    self.mainWidget = MainWidget(self)
    self.setCentralWidget(self.mainWidget)
    self.statusBar().showMessage('Status: Ready')
    self.setGeometry(1280, 720, 1280, 720)
    self.showMaximized()

class MainWidget(QWidget):
  def __init__(self, parent):
    super().__init__()
    self.db = Database()
    self.parent = parent
    self.initUI()

  def initUI(self):
    self.grading = False
    self.box = QGridLayout(self)
    font = QFont("Times", 18, QFont.Bold)
    self.titleLabel = QLabel("MIPS Grader Application")
    self.titleLabel.setFont(font)
    self.titleLabel.setAlignment(Qt.AlignCenter)
    self.box.addWidget(self.titleLabel, 0, 0, 1, 2)
    self.clearDBbtn = QPushButton("Clear Database")
    self.clearDBbtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_DialogResetButton")))
    self.clearDBbtn.clicked.connect(self.clearDataBase)
    self.box.addWidget(self.clearDBbtn, 0, 2,)

    # sections layout
    self.sectionLayout = QGridLayout(self)
    self.sectionLayout.addWidget(QLabel("Sections:"), 0, 0)
    self.addSectionBtn = QPushButton("Add")
    self.addSectionBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_FileDialogNewFolder")))
    self.addSectionBtn.clicked.connect(self.addSection)
    self.addSectionBtn.setToolTip("Add Section")
    self.sectionLayout.addWidget(self.addSectionBtn, 0, 1)
    self.editSectionBtn = QPushButton("Edit")
    self.editSectionBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_FileIcon")))
    self.editSectionBtn.clicked.connect(self.editSection)
    self.editSectionBtn.setToolTip("Edit Section")
    self.sectionLayout.addWidget(self.editSectionBtn, 0, 2)
    self.delSectionBtn = QPushButton("Delete")
    self.delSectionBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_MessageBoxCritical")))
    self.delSectionBtn.clicked.connect(self.delSection)
    self.delSectionBtn.setToolTip("Delete Section")
    self.sectionLayout.addWidget(self.delSectionBtn, 0, 3)
    self.sectionListWidget = QListWidget()
    self.updateSections()
    self.sectionListWidget.itemClicked.connect(self.sectionClicked)
    self.sectionLayout.addWidget(self.sectionListWidget, 1, 0, 1, 4)
    self.box.addLayout(self.sectionLayout, 1, 0)

    self.tabs = QTabWidget()
    self.tabs.currentChanged.connect(self.tabChanged)
    self.tab1 = QWidget()
    self.tab2 = QWidget()
    self.tab3 = QWidget()

    self.tabs.addTab(self.tab1, "Labs")
    self.tabs.addTab(self.tab2, "Students")
    self.tabs.addTab(self.tab3, "Grades")

    # Lab Tab
    self.labLayout = QGridLayout(self)
    self.labLayout.addWidget(QLabel("Labs:"), 0, 0)
    self.labLayout.addWidget(QLabel("Tasks:"), 0, 2)
    self.labBtnsLayout = QGridLayout(self)
    self.addLabBtn = QPushButton("Add")
    self.addLabBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_FileDialogNewFolder")))
    self.addLabBtn.clicked.connect(self.addLab)
    self.addLabBtn.setToolTip("Add Lab")
    self.labBtnsLayout.addWidget(self.addLabBtn, 0, 0)
    self.editLabBtn = QPushButton("Edit")
    self.editLabBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_FileIcon")))
    self.editLabBtn.clicked.connect(self.editLab)
    self.editLabBtn.setToolTip("Edit Lab")
    self.labBtnsLayout.addWidget(self.editLabBtn, 0, 1)
    self.delLabBtn = QPushButton("Delete")
    self.delLabBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_MessageBoxCritical")))
    self.delLabBtn.clicked.connect(self.delLab)
    self.delLabBtn.setToolTip("Delete Lab")
    self.labBtnsLayout.addWidget(self.delLabBtn, 0, 2)
    self.gradeLabBtn = QPushButton("Grade Lab")
    self.gradeLabBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_FileDialogContentsView")))
    self.gradeLabBtn.clicked.connect(self.gradeLab)
    self.labBtnsLayout.addWidget(self.gradeLabBtn, 1, 0, 1, 3)
    self.copyLabBtn = QPushButton("Copy Lab")
    self.copyLabBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_FileDialogDetailedView")))
    self.copyLabBtn.clicked.connect(self.copyLab)
    self.labBtnsLayout.addWidget(self.copyLabBtn, 2, 0, 1, 3)
    self.labLayout.addLayout(self.labBtnsLayout, 0, 1)
    self.labListWidget = QListWidget()
    self.updateLabs()
    self.labListWidget.itemClicked.connect(self.labClicked)
    self.labLayout.addWidget(self.labListWidget, 1, 0, 1, 2)



    # Tasks Tabs
    self.tasksTabs = QTabWidget()
    self.tasksTabs.currentChanged.connect(self.taskChanged)
    self.taskBtnsLayout = QGridLayout(self)
    self.addTaskBtn = QPushButton("Add")
    self.addTaskBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_FileDialogNewFolder")))
    self.addTaskBtn.clicked.connect(self.addTask)
    self.addTaskBtn.setToolTip("Add Task")
    self.taskBtnsLayout.addWidget(self.addTaskBtn, 0, 0)
    self.editTaskBtn = QPushButton("Edit")
    self.editTaskBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_FileIcon")))
    self.editTaskBtn.clicked.connect(self.editTask)
    self.editTaskBtn.setToolTip("Edit Task")
    self.taskBtnsLayout.addWidget(self.editTaskBtn, 0, 1)
    self.delTaskBtn = QPushButton("Delete")
    self.delTaskBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_MessageBoxCritical")))
    self.delTaskBtn.clicked.connect(self.delTask)
    self.delTaskBtn.setToolTip("Delete Task")
    self.taskBtnsLayout.addWidget(self.delTaskBtn, 0, 2)
    self.labLayout.addLayout(self.taskBtnsLayout, 0, 3)

    self.labLayout.addWidget(self.tasksTabs, 1, 2, 1, 2)
    self.labLayout.setColumnStretch(2, 3)
    self.tab1.setLayout(self.labLayout)

    # Students Tab
    self.studentLayout = QGridLayout(self)
    self.studentLayout.addWidget(QLabel("Students:"), 0, 0)
    self.addStudentBtn = QPushButton("Add")
    self.addStudentBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_FileDialogNewFolder")))
    self.addStudentBtn.clicked.connect(self.addStudent)
    self.addStudentBtn.setToolTip("Add Student")
    self.studentLayout.addWidget(self.addStudentBtn, 0, 1)
    self.delStudentBtn = QPushButton("Delete")
    self.delStudentBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_MessageBoxCritical")))
    self.delStudentBtn.clicked.connect(self.delStudent)
    self.delStudentBtn.setToolTip("Delete Student")
    self.studentLayout.addWidget(self.delStudentBtn, 0, 2)
    self.saveStudentBtn = QPushButton("Save")
    self.saveStudentBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_DialogSaveButton")))
    self.saveStudentBtn.clicked.connect(self.saveStudents)
    self.saveStudentBtn.setToolTip("Save Changes")
    self.studentLayout.addWidget(self.saveStudentBtn, 0, 3)
    self.importStudentBtn = QPushButton("Import from CSV file")
    self.importStudentBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_DialogOpenButton")))
    self.importStudentBtn.clicked.connect(self.importStudents)
    self.importStudentBtn.setToolTip("Import from CSV file")
    self.studentLayout.addWidget(self.importStudentBtn, 0, 4)
    self.studentTable = QTableWidget()
    self.studentTable.setColumnCount(3)
    self.studentTable.setHorizontalHeaderItem(0, QTableWidgetItem("ID"))
    self.studentTable.setHorizontalHeaderItem(1, QTableWidgetItem("First Name"))
    self.studentTable.setHorizontalHeaderItem(2, QTableWidgetItem("Last Name"))
    self.studentTable.horizontalHeader().setStretchLastSection(True)
    self.studentTable.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
    self.studentLayout.addWidget(self.studentTable, 1, 0, 1, 5)
    self.tab2.setLayout(self.studentLayout)

    # Grades Tab
    self.gradesLayout = QGridLayout(self)
    self.gradesLayout.addWidget(QLabel("Grades:"), 0, 0)
    self.exportGradesBtn = QPushButton("Export Grades to CSV file")
    self.exportGradesBtn.setIcon(self.style().standardIcon(getattr(QStyle, "SP_DialogSaveButton")))
    self.exportGradesBtn.clicked.connect(self.exportGrades)
    self.exportGradesBtn.setToolTip("Export Grades to CSV file")
    self.gradesLayout.addWidget(self.exportGradesBtn, 0, 1)
    self.gradesTable = QTableWidget()
    self.gradesTable.setColumnCount(3)
    self.gradesTable.setHorizontalHeaderItem(0, QTableWidgetItem("ID"))
    self.gradesTable.setHorizontalHeaderItem(1, QTableWidgetItem("First Name"))
    self.gradesTable.setHorizontalHeaderItem(2, QTableWidgetItem("Last Name"))
    self.gradesTable.horizontalHeader().setStretchLastSection(True)
    self.gradesTable.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
    self.gradesLayout.addWidget(self.gradesTable, 1, 0, 1, 2)
    self.tab3.setLayout(self.gradesLayout)

    # Add tab windows to gridlayout of window
    self.box.addWidget(self.tabs, 1, 1, 1, 2)
    self.box.setColumnStretch(1, 3)
    self.box.setRowStretch(1, 10)
    self.layout = self.box

    self.progressBar = QProgressBar(self)
    self.progressBar.setGeometry(30, 40, 200, 25)
    self.progressBar.setValue(0)
    self.parent.statusBar().addPermanentWidget(self.progressBar)
    

  def tabChanged(self, tabIndex):
    if not self.grading:
      if tabIndex == 0:
        self.parent.statusBar().showMessage('Status: Lab Tab')
      elif tabIndex == 1:
        self.parent.statusBar().showMessage('Status: Students Tab')
      elif tabIndex == 2:
        self.parent.statusBar().showMessage('Status: Grades Tab')
        self.updateGrades()

  def taskChanged(self, tabIndex):
    if hasattr(self, 'tasksTabs'):
      self.updateTestCases()
    if not self.grading:
      self.parent.statusBar().showMessage('Status: {} selected'.format(self.tasksTabs.tabText(tabIndex)))

  def updateSections(self):
    self.sectionListWidget.clear()
    for section in Section.getSections(self.db):
      self.sectionListWidget.addItem(section.__str__())
    self.sectionListWidget.sortItems(Qt.AscendingOrder)

  def updateLabs(self):
    self.labListWidget.clear()
    sections = self.sectionListWidget.selectedItems()
    if sections:
      sectionStr = sections[0].text()
      sectionid = sectionStr.split("_")[0]
      for lab in Lab.getLabsOfSection(self.db, sectionid):
        self.labListWidget.addItem(lab.__str__())
      self.labListWidget.sortItems(Qt.AscendingOrder)

  def sectionClicked(self, item):
    if not self.grading:
      self.parent.statusBar().showMessage('Status: {} selected'.format(item.text().split("_")[1]))
    self.updateLabs()
    self.updateStudents()
    self.updateGrades()
    self.updateTasks()

  def labClicked(self, item):
    if not self.grading:
      self.parent.statusBar().showMessage('Status: {} selected'.format(item.text().split("_")[1]))
    self.updateTasks()

  def updateTasks(self):
    self.tasksTabs.clear()
    labs = self.labListWidget.selectedItems()
    if labs:
      labid = labs[0].text().split("_")[0]
      tasks = Task.getTasksOfLab(self.db, labid)
      i = 1
      for task in tasks:
        tab = QWidget()
        self.tasksTabs.addTab(tab, "Task {}".format(i))
        layout = QGridLayout(self)
        layout.addWidget(QLabel("TaskID: "), 0, 0)
        layout.addWidget(QLabel("{}".format(task.taskid)), 0, 1)
        layout.addWidget(QLabel("Name: "), 1, 0)
        layout.addWidget(QLabel(task.name), 1, 1)
        layout.addWidget(QLabel("Weight: "), 2, 0)
        layout.addWidget(QLabel("{}".format(task.weight)), 2, 1)
        testCasesTable = QTableWidget()
        layout.addWidget(QLabel("TestCases: "), 3, 0)
        btnLayout = QGridLayout(self)
        addTestCase = QPushButton("Add")
        addTestCase.setIcon(self.style().standardIcon(getattr(QStyle, "SP_FileDialogNewFolder")))
        addTestCase.clicked.connect(self.addTestCase)
        addTestCase.setToolTip("Add Test Case")
        btnLayout.addWidget(addTestCase, 0, 0)
        editTestCase = QPushButton("Edit")
        editTestCase.setIcon(self.style().standardIcon(getattr(QStyle, "SP_FileIcon")))
        editTestCase.clicked.connect(self.editTestCase)
        editTestCase.setToolTip("Edit Test Task")
        btnLayout.addWidget(editTestCase, 0, 1)
        delTestCase = QPushButton("Delete")
        delTestCase.setIcon(self.style().standardIcon(getattr(QStyle, "SP_MessageBoxCritical")))
        delTestCase.clicked.connect(self.delTestCase)
        delTestCase.setToolTip("Delete Test Case")
        btnLayout.addWidget(delTestCase, 0, 2)
        layout.addLayout(btnLayout, 3, 1)
        layout.addWidget(testCasesTable, 4, 1)
        testCasesTable.horizontalHeader().setStretchLastSection(True)
        testCasesTable.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        tab.setLayout(layout)
        i += 1
      self.updateTestCases()

  def updateTestCases(self):
    tab = self.tasksTabs.currentWidget()
    if tab:
      elements = tab.children()
      if len(elements) > 8:
        taskid = elements[2].text()
        testCasesTable = elements[-1]
        testCasesTable.clear()
        testCasesTable.setColumnCount(4)
        testCasesTable.setHorizontalHeaderItem(0, QTableWidgetItem("ID"))
        testCasesTable.setHorizontalHeaderItem(1, QTableWidgetItem("Name"))
        testCasesTable.setHorizontalHeaderItem(2, QTableWidgetItem("Arguments"))
        testCasesTable.setHorizontalHeaderItem(3, QTableWidgetItem("Expected Output"))
        testCases = TestCase.getTestCasesOfTask(self.db, taskid)
        testCasesTable.setRowCount(len(testCases))
        for index, testCase in enumerate(testCases):
          idItem = QTableWidgetItem(str(testCase.testcaseid))
          nameItem = QTableWidgetItem(testCase.name)
          argsItem = QTableWidgetItem(testCase.arguments)
          outItem = QTableWidgetItem(testCase.expected_output)
          # Make cells uneditable
          idItem.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
          nameItem.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
          argsItem.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
          outItem.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
          testCasesTable.setItem(index, 0, idItem)
          testCasesTable.setItem(index, 1, nameItem)
          testCasesTable.setItem(index, 2, argsItem)
          testCasesTable.setItem(index, 3, outItem)
        testCasesTable.setSortingEnabled(True)
        testCasesTable.sortByColumn(0, Qt.AscendingOrder)  # sort by the first column

  def updateStudents(self):
    self.studentTable.clear()
    sections = self.sectionListWidget.selectedItems()
    if sections:
      self.studentTable.setHorizontalHeaderItem(0, QTableWidgetItem("ID"))
      self.studentTable.setHorizontalHeaderItem(1, QTableWidgetItem("First Name"))
      self.studentTable.setHorizontalHeaderItem(2, QTableWidgetItem("Last Name"))
      section_str = sections[0].text()
      sectionid = section_str.split("_")[0]
      students = Student.getStudentsOfSection(self.db, sectionid)
      self.studentTable.setRowCount(len(students))
      for index, student in enumerate(students):
        self.studentTable.setItem(index, 0, QTableWidgetItem(str(student.studentid)))
        self.studentTable.setItem(index, 1, QTableWidgetItem(student.fname))
        self.studentTable.setItem(index, 2, QTableWidgetItem(student.lname))
    self.studentTable.setSortingEnabled(True)
    self.studentTable.sortByColumn(0, Qt.AscendingOrder)

  def updateGrades(self):
    self.gradesTable.clear()
    sections = self.sectionListWidget.selectedItems()
    if sections:
      section_str = sections[0].text()
      sectionid = section_str.split("_")[0]
      labs = Lab.getLabsOfSection(self.db, sectionid)
      self.gradesTable.setColumnCount(3+len(labs))
      self.gradesTable.setHorizontalHeaderItem(0, QTableWidgetItem("ID"))
      self.gradesTable.setHorizontalHeaderItem(1, QTableWidgetItem("First Name"))
      self.gradesTable.setHorizontalHeaderItem(2, QTableWidgetItem("Last Name"))
      i = 3
      for lab in labs:
        self.gradesTable.setHorizontalHeaderItem(i, QTableWidgetItem(lab.name))
        i += 1
      students = Student.getStudentsOfSection(self.db, sectionid)
      self.gradesTable.setRowCount(len(students))
      for index, student in enumerate(students):
        idItem = QTableWidgetItem(str(student.studentid))
        fnameItem = QTableWidgetItem(student.fname)
        lnameItem = QTableWidgetItem(student.lname)
        idItem.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        fnameItem.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        lnameItem.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        self.gradesTable.setItem(index, 0, idItem)
        self.gradesTable.setItem(index, 1, fnameItem)
        self.gradesTable.setItem(index, 2, lnameItem)
        i = 3
        for lab in labs:
          labgrade = Lab_Grade.getLab_GradeOfStudentAndLab(self.db, student.studentid, lab.labid)
          if labgrade is not None:
            labItem = QTableWidgetItem(str(labgrade.grade))
          else:
            labItem = QTableWidgetItem("Not Graded")
          labItem.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
          self.gradesTable.setItem(index, i, labItem)
          i += 1
      self.gradesTable.setSortingEnabled(True)
      self.gradesTable.sortByColumn(0, Qt.AscendingOrder)  # sort by the first column


  def addSection(self):
    sectionForm = SectionForm()
    result = sectionForm.exec_()
    if result == 1:
      data = sectionForm.getData()
      section = Section(-1, data[1], data[2])
      section.save(self.db)
      self.updateSections()
      if not self.grading:
        self.parent.statusBar().showMessage('Status: Section added successfully')

  def editSection(self):
    sections = self.sectionListWidget.selectedItems()
    if sections:
      section_str = sections[0].text()
      section_parts = section_str.split("_")
      sectionForm = SectionForm(section_parts[0], section_parts[1], section_parts[2])
      result = sectionForm.exec_()
      if result == 1:
        data = sectionForm.getData()
        section = Section(data[0], data[1], data[2])
        section.save(self.db)
        self.updateSections()
        if not self.grading:
          self.parent.statusBar().showMessage('Status: Section updated successfully')
    else:
      QMessageBox.information(self, "Information", "Please select a section first", buttons=QMessageBox.Ok)

  def delSection(self):
    sections = self.sectionListWidget.selectedItems()
    if sections:
      section_str = sections[0].text()
      section_parts = section_str.split("_")
      section = Section(section_parts[0], section_parts[1], section_parts[2])
      response = QMessageBox.information(self, "Critical", "Are you sure you want to delete section {}".format(section.__str__()),
                                         buttons=QMessageBox.Yes | QMessageBox.No)
      if response == QMessageBox.Yes:
        section.delete(self.db)
        self.updateSections()
        if not self.grading:
          self.parent.statusBar().showMessage('Status: Section deleted successfully')
    else:
      QMessageBox.information(self, "Information", "Please select a section first", buttons=QMessageBox.Ok)

  def addLab(self):
    sections = self.sectionListWidget.selectedItems()
    if sections:
      section_str = sections[0].text()
      sectionid = section_str.split("_")[0]
      labForm = LabForm(sectionid=sectionid)
      result = labForm.exec_()
      if result == 1:
        data = labForm.getData()
        lab = Lab(data[0], data[1], data[2])
        lab.save(self.db)
        self.updateLabs()
        if not self.grading:
          self.parent.statusBar().showMessage('Status: Lab added successfully')
    else:
      QMessageBox.information(self, "Information", "Please select a section first", buttons=QMessageBox.Ok)

  def editLab(self):
    sections = self.sectionListWidget.selectedItems()
    if sections:
      section_str = sections[0].text()
      sectionid = section_str.split("_")[0]
      labs = self.labListWidget.selectedItems()
      if labs:
        labid, labname = labs[0].text().split("_")
        labForm = LabForm(id = labid, name=labname, sectionid=sectionid)
        result = labForm.exec_()
        if result == 1:
          data = labForm.getData()
          lab = Lab(data[0], data[1], data[2])
          lab.save(self.db)
          self.updateLabs()
          if not self.grading:
            self.parent.statusBar().showMessage('Status: Lab updated successfully')
      else:
        QMessageBox.information(self, "Information", "Please select a lab first", buttons=QMessageBox.Ok)
    else:
      QMessageBox.information(self, "Information", "Please select a section first", buttons=QMessageBox.Ok)

  def delLab(self):
    sections = self.sectionListWidget.selectedItems()
    if sections:
      section_str = sections[0].text()
      sectionid = section_str.split("_")[0]
      labs = self.labListWidget.selectedItems()
      if labs:
        labid, labname = labs[0].text().split("_")
        lab = Lab(labid, labname, sectionid)
        response = QMessageBox.information(self, "Critical", "Are you sure you want to delete lab {}".format(lab.__str__()),
                                           buttons=QMessageBox.Yes | QMessageBox.No)
        if response == QMessageBox.Yes:
          lab.delete(self.db)
          self.updateLabs()
          if not self.grading:
            self.parent.statusBar().showMessage('Status: Lab deleted successfully')
      else:
        QMessageBox.information(self, "Information", "Please select a lab first", buttons=QMessageBox.Ok)
    else:
      QMessageBox.information(self, "Information", "Please select a section first", buttons=QMessageBox.Ok)

  def addStudent(self):
    sections = self.sectionListWidget.selectedItems()
    if sections:
      section_str = sections[0].text()
      sectionid = section_str.split("_")[0]
      studentForm = StudentForm(sectionid)
      result = studentForm.exec_()
      if result == 1:
        data = studentForm.getData()
        student = Student(data[0], data[1], data[2], data[3])
        student.save(self.db)
        self.updateStudents()
        if not self.grading:
          self.parent.statusBar().showMessage('Status: Student added successfully')
    else:
      QMessageBox.information(self, "Information", "Please select a section first", buttons=QMessageBox.Ok)
  def importStudents(self):
    sections = self.sectionListWidget.selectedItems()
    if sections:
      section_str = sections[0].text()
      sectionid = section_str.split("_")[0]
      options = QFileDialog.Options() | QFileDialog.ExistingFile
      filename,_ = QFileDialog.getOpenFileName(self.parent,"QFileDialog.getOpenFileName()", "","CSV files (*.csv)", options=options)
      if filename:
        csvfile = open(filename)
        students_info = csv.DictReader(csvfile, delimiter=",")
        username_field = [i for i in students_info.fieldnames if "Username" in i]
        if username_field:
          username_field = username_field[0]
        else:
          QMessageBox.information(self, "Critical", "Could not find Username column", buttons=QMessageBox.Ok)
          return
        first_name_field = [i for i in students_info.fieldnames if "First Name" in i]
        if first_name_field:
          first_name_field = first_name_field[0]
        else:
          QMessageBox.information(self, "Critical", "Could not find First Name column", buttons=QMessageBox.Ok)
          return
        last_name_field = [i for i in students_info.fieldnames if "Last Name" in i]
        if last_name_field:
          last_name_field = last_name_field[0]
        else:
          QMessageBox.information(self, "Critical", "Could not find Last Name column", buttons=QMessageBox.Ok)
          return
        for row in students_info:
          student_id = row[username_field][1:]
          student = Student(student_id, row[first_name_field], row[last_name_field], sectionid)
          student.save(self.db)
        self.updateStudents()
        if not self.grading:
          self.parent.statusBar().showMessage('Status: Students imported successfully')
        QMessageBox.information(self, "Information", "Students imported successfully", buttons=QMessageBox.Ok)
    else:
      QMessageBox.information(self, "Information", "Please select a section first", buttons=QMessageBox.Ok)
  def saveStudents(self):
    sections = self.sectionListWidget.selectedItems()
    if sections:
      section_str = sections[0].text()
      sectionid = section_str.split("_")[0]
      for row in range(self.studentTable.rowCount()):
        id = self.studentTable.item(row, 0).text()
        fname = self.studentTable.item(row, 1).text()
        lname = self.studentTable.item(row, 2).text()
        student = Student(id, fname, lname, sectionid, True)
        student.save(self.db)
        if not self.grading:
          self.parent.statusBar().showMessage('Status: Students saved successfully')
    else:
      QMessageBox.information(self, "Information", "Please select a section first", buttons=QMessageBox.Ok)
  def delStudent(self):
    sections = self.sectionListWidget.selectedItems()
    if sections:
      section_str = sections[0].text()
      sectionid = section_str.split("_")[0]
      rowSelected = self.studentTable.currentRow()
      if rowSelected != -1:
        id = self.studentTable.item(rowSelected, 0).text()
        fname = self.studentTable.item(rowSelected, 1).text()
        lname = self.studentTable.item(rowSelected, 2).text()
        student = Student(id, fname, lname, sectionid, True)
        response = QMessageBox.information(self, "Critical", "Are you sure you want to delete student {}".format(student.__str__()),
                                           buttons=QMessageBox.Yes | QMessageBox.No)
        if response == QMessageBox.Yes:
          student.delete(self.db)
          self.updateStudents()
          if not self.grading:
            self.parent.statusBar().showMessage('Status: Student deleted successfully')
    else:
      QMessageBox.information(self, "Information", "Please select a section first", buttons=QMessageBox.Ok)

  def exportGrades(self):
    sections = self.sectionListWidget.selectedItems()
    if sections:
      section_str = sections[0].text()
      sectionid = section_str.split("_")[0]
      options = QFileDialog.Options()
      filename,_ = QFileDialog.getSaveFileName(self.parent,"QFileDialog.getSaveFileName()", "","CSV File (*.csv)", options=options)
      if filename:
        labs = Lab.getLabsOfSection(self.db, sectionid)
        fw = open(filename, "w")
        fw.write("\"Username\",\"First Name\",\"Last Name\"")
        for lab in labs:
          fw.write(",\"{}\"".format(lab.name))
        fw.write("\n")
        rows = self.gradesTable.rowCount()
        cols = self.gradesTable.columnCount()
        for row in range(rows):
          row_str = ""
          for col in range(cols):
            cell = self.gradesTable.item(row, col)
            if col == 0:
              row_str += "\"s{}\",".format(cell.text())
            else:
              row_str += "\"{}\",".format(cell.text())
          row_str = row_str[:-1] + "\n"
          fw.write(row_str)
        fw.close()
        if not self.grading:
          self.parent.statusBar().showMessage('Status: Grades exported successfully')
    else:
      QMessageBox.information(self, "Information", "Please select a section first", buttons=QMessageBox.Ok)

  def addTask(self):
    labs = self.labListWidget.selectedItems()
    if labs:
      labid = labs[0].text().split("_")[0]
      taskForm = TaskForm(labid=labid)
      result = taskForm.exec_()
      if result == 1:
        data = taskForm.getData()
        task = Task(data[0], data[1], data[2], data[3])
        task.save(self.db)
        self.updateTasks()
        if not self.grading:
          self.parent.statusBar().showMessage('Status: Task added successfully')
    else:
      QMessageBox.information(self, "Information", "Please select a lab first", buttons=QMessageBox.Ok)

  def editTask(self):
    labs = self.labListWidget.selectedItems()
    if labs:
      labid = labs[0].text().split("_")[0]
      tab = self.tasksTabs.currentWidget()
      if tab:
        elements = tab.children()
        taskid = elements[2].text()
        name = elements[4].text()
        weight = elements[6].text()
        taskForm = TaskForm(id=taskid, name=name, weight=weight, labid=labid)
        result = taskForm.exec_()
        if result == 1:
          data = taskForm.getData()
          task = Task(data[0], data[1], data[2], data[3])
          task.save(self.db)
          self.updateTasks()
          if not self.grading:
            self.parent.statusBar().showMessage('Status: Task updated successfully')
      else:
        QMessageBox.information(self, "Information", "Please select a task first", buttons=QMessageBox.Ok)
    else:
      QMessageBox.information(self, "Information", "Please select a lab first", buttons=QMessageBox.Ok)
  def delTask(self):
    labs = self.labListWidget.selectedItems()
    if labs:
      labid = labs[0].text().split("_")[0]
      tab = self.tasksTabs.currentWidget()
      if tab:
        elements = tab.children()
        taskid = elements[2].text()
        name = elements[4].text()
        weight = elements[6].text()
        task = Task(taskid=taskid, labid=labid, name=name, weight=weight)
        response = QMessageBox.information(self, "Critical", "Are you sure you want to delete task {}".format(task.__str__()),
                                           buttons=QMessageBox.Yes | QMessageBox.No)
        if response == QMessageBox.Yes:
          task.delete(self.db)
          self.updateTasks()
          if not self.grading:
            self.parent.statusBar().showMessage('Status: Task deleted successfully')
      else:
        QMessageBox.information(self, "Information", "Please select a task first", buttons=QMessageBox.Ok)
    else:
      QMessageBox.information(self, "Information", "Please select a lab first", buttons=QMessageBox.Ok)

  def addTestCase(self):
    tab = self.tasksTabs.currentWidget()
    if tab:
      elements = tab.children()
      if len(elements) > 8:
        taskid = elements[2].text()
        testCaseForm = TestCaseForm(taskid=taskid)
        result = testCaseForm.exec_()
        if result == 1:
          data = testCaseForm.getData()
          testCase = TestCase(data[0], data[1], data[2], data[3], data[4])
          testCase.save(self.db)
          self.updateTestCases()
          if not self.grading:
            self.parent.statusBar().showMessage('Status: Test Case added successfully')
  def editTestCase(self):
    tab = self.tasksTabs.currentWidget()
    if tab:
      elements = tab.children()
      if len(elements) > 8:
        taskid = elements[2].text()
        testCasesTable = elements[-1]
        row = testCasesTable.currentRow()
        if row >= 0:
          testcaseid = testCasesTable.item(row, 0).text()
          name = testCasesTable.item(row, 1).text()
          arguments = testCasesTable.item(row, 2).text()
          expected_output = testCasesTable.item(row, 3).text()
          testCaseForm = TestCaseForm(
              id=testcaseid, name=name, arguments=arguments, expected_output=expected_output, taskid=taskid)
          result = testCaseForm.exec_()
          if result == 1:
            data = testCaseForm.getData()
            testCase = TestCase(data[0], data[1], data[2], data[3], data[4])
            testCase.save(self.db)
            self.updateTestCases()
            if not self.grading:
              self.parent.statusBar().showMessage('Status: Test Case updated successfully')
  def delTestCase(self):
    tab = self.tasksTabs.currentWidget()
    if tab:
      elements = tab.children()
      if len(elements) > 8:
        taskid = elements[2].text()
        testCasesTable = elements[-1]
        row = testCasesTable.currentRow()
        if row >= 0:
          testcaseid = testCasesTable.item(row, 0).text()
          name = testCasesTable.item(row, 1).text()
          arguments = testCasesTable.item(row, 2).text()
          expected_output = testCasesTable.item(row, 3).text()
          testCase = TestCase(testcaseid, name, arguments, expected_output, taskid)
          response = QMessageBox.information(self, "Critical", "Are you sure you want to delete test case {}".format(testCase.__str__()),
                                             buttons=QMessageBox.Yes | QMessageBox.No)
          if response == QMessageBox.Yes:
            testCase.delete(self.db)
            self.updateTestCases()
            if not self.grading:
              self.parent.statusBar().showMessage('Status: Test Case deleted successfully')
  def gradeLab(self):
    sections = self.sectionListWidget.selectedItems()
    if sections:
      sectionid = sections[0].text().split("_")[0]
      labs = self.labListWidget.selectedItems()
      if labs:
        labid = labs[0].text().split("_")[0]
        labname = labs[0].text().split("_")[1]
        self.parent.statusBar().showMessage('Status: Please wait while grading {}'.format(labname))
        folderPath = QFileDialog.getExistingDirectory(self.parent, "Select Lab folder to grade", "", options=QFileDialog.ShowDirsOnly)
        if folderPath:
          # Creating QThread object
          self.grading = True
          self.thread = QThread()
          # Creating worker object
          self.gradingWorker = GraderQWorker(self, self.db, labid, sectionid, folderPath)
          # moving worker to thread
          self.gradingWorker.moveToThread(self.thread)
          # connect signals
          self.thread.started.connect(self.gradingWorker.run)
          self.gradingWorker.finished.connect(self.thread.quit)
          self.gradingWorker.finished.connect(self.gradingWorker.deleteLater)
          self.thread.finished.connect(self.thread.deleteLater)
          self.gradingWorker._workDoneSignal.connect(self.update_progress_bar)
          # Start the thread
          self.thread.start()
          # Final resets
          self.gradeLabBtn.setEnabled(False)
          self.thread.finished.connect(lambda: self.finishedGrading(labname))
    else:
      QMessageBox.information(self, "Information", "Please select a section", buttons=QMessageBox.Ok)
  def update_progress_bar(self, value):
    self.progressBar.setValue(int(value))
  def finishedGrading(self, labname):
    self.gradeLabBtn.setEnabled(True)
    self.grading = False
    self.progressBar.setValue(100)
    self.parent.statusBar().showMessage('Status: Finished grading {}'.format(labname))
    QMessageBox.information(self, "Information", "Finished Grading {}".format(labname), buttons=QMessageBox.Ok)
  def copyLab(self):
    sections = self.sectionListWidget.selectedItems()
    if sections:
      current_section = sections[0].text()
      sectionid = current_section.split("_")[0]
      labs = self.labListWidget.selectedItems()
      if labs:
        labid = labs[0].text().split("_")[0]
        labname = labs[0].text().split("_")[1]
        unselectedSectionsStr = []
        for x in range(self.sectionListWidget.count()):
          section_name = self.sectionListWidget.item(x).text()
          if section_name != current_section:
            unselectedSectionsStr.append(section_name)
        copyLabForm = CopyLabForm(labid, labname, unselectedSectionsStr)
        result = copyLabForm.exec_()
        if result == 1:
          data = copyLabForm.getData()
          destination_sections = data[2]
          for destination_section in destination_sections:
            self.copyLabToSection(labid, labname, destination_section)
          if not self.grading:
            self.parent.statusBar().showMessage('Status: Finished Copying {}'.format(labname))
          QMessageBox.information(self, "Information", "Finished Copying {}".format(labname), buttons=QMessageBox.Ok)
      else:
        QMessageBox.information(self, "Information", "Please select a lab", buttons=QMessageBox.Ok)
    else:
      QMessageBox.information(self, "Information", "Please select a section", buttons=QMessageBox.Ok)

  def copyLabToSection(self, labid, labname, section):
    sectionid = section.split("_")[0]
    newLab = Lab(-1, name=labname, sectionid=sectionid)
    newLab.save(self.db)
    tasks = Task.getTasksOfLab(self.db, labid=labid)
    for task in tasks:
      newTask = Task(-1, newLab.labid, task.name, task.weight)
      newTask.save(self.db)
      testCases = TestCase.getTestCasesOfTask(self.db, task.taskid)
      for testCase in testCases:
        newTestCase = TestCase(-1, testCase.name, testCase.arguments, testCase.expected_output, newTask.taskid)
        newTestCase.save(self.db)

  def clearDataBase(self):
    if not self.grading:
      response = QMessageBox.information(self, "Critical", """Are you sure you want to clear the database?
                                         (all sections, labs, students, tasks, testcases) information will be deleted""",
                                         buttons=QMessageBox.Yes | QMessageBox.No)
      if response == QMessageBox.Yes:
        lab_grades = Lab_Grade.getLab_Grades(self.db)
        for lab_grade in lab_grades:
          lab_grade.delete(self.db)
        test_cases = TestCase.getTestCases(self.db)
        for test_case in test_cases:
          test_case.delete(self.db)
        tasks = Task.getTasks(self.db)
        for task in tasks:
          task.delete(self.db)
        labs = Lab.getLabs(self.db)
        for lab in labs:
          lab.delete(self.db)
        students = Student.getStudents(self.db)
        for student in students:
          student.delete(self.db)
        sections = Section.getSections(self.db)
        for section in sections:
          section.delete(self.db)
        self.updateSections()
        self.db.execute("DELETE FROM sqlite_sequence")
        self.db.commit()
        self.parent.statusBar().showMessage('Status: Database has been cleared')
        QMessageBox.information(self, "Information", "Database has been cleared", buttons=QMessageBox.Ok)
