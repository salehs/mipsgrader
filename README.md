MIPS Grader Application
==============================

<!-- [![TravisCI](https://travis-ci.org/uber/Python-Sample-Application.svg?branch=master)](https://travis-ci.org/uber/Python-Sample-Application) -->
<!-- [![Coverage Status](https://coveralls.io/repos/uber/Python-Sample-Application/badge.png)](https://coveralls.io/r/uber/Python-Sample-Application) -->


What Is This?
-------------
This is a graphical based application to grade MIPS assignment using [MARS](http://courses.missouristate.edu/kenvollmar/mars/) tool. 

How To Use This
---------------
Clone this repository and run the application from 
`dist/MIPSGrader.exe`.

Currently this application only supports Windows Operating System. 



Project Organization
------------

    ├── README.md          <- The top-level README for developers using this project.
    ├── dist               <- Compiled version of the application for Windows OS
    ├── forms              <- A set of simple python classes to be used for the forms of the application
    │
    ├── models             <- Contains python classes to communicate with database for inserting, updating, and deleting sections, labs, students, etc. 
    │
    ├── requirements.txt   <- The requirements file for running the application
    │
    ├── Database.py        <- Database class handles communication with sqlite database
    │
    ├── MainWindow.py      <- Python class that displays the main window of the application and draws its components
    ├── Mars4_5.jar        <- Mars tool used to assemble and run students' submissions
    │
    └── main.py            <- Main python file that starts the application