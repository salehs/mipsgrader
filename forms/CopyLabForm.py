"""
CopyLabForm.py - CopyLabForm class for MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *


class CopyLabForm(QDialog):
  def __init__(self, labid, labname, sections):
    super(CopyLabForm, self).__init__()

    self.labid = labid
    self.labname = labname
    self.sections = sections

    self.createFormGroupBox()
    buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
    buttonBox.accepted.connect(self.accept)
    buttonBox.rejected.connect(self.reject)

    mainLayout = QVBoxLayout()
    mainLayout.addWidget(self.formGroupBox)
    mainLayout.addWidget(buttonBox)
    self.setLayout(mainLayout)

    self.setWindowTitle("Copy Lab Form")

  def createFormGroupBox(self):
    self.formGroupBox = QGroupBox("Copy Lab {} to".format(self.labname))
    self.layout = QFormLayout()
    self.sectionsCheckBox = []
    self.checkAll = QCheckBox("Select All")
    self.checkAll.stateChanged.connect(self.check)
    self.checkAll.setChecked(True)
    self.layout.addRow(self.checkAll)
    for section in self.sections:
      checkBox = QCheckBox(section)
      checkBox.setChecked(True)
      checkBox.stateChanged.connect(self.check)
      self.layout.addRow(checkBox)
      self.sectionsCheckBox.append(checkBox)
    self.formGroupBox.setLayout(self.layout)

  def check(self, state):
    # checking if state is checked
    self.checkAll.blockSignals(True)
    for checkbox in self.sectionsCheckBox:
      checkbox.blockSignals(True)
    if state == Qt.Checked:
      if self.sender() == self.checkAll:
        for checkbox in self.sectionsCheckBox:
          checkbox.setChecked(True)
      else:
        allChecked = True
        for checkbox in self.sectionsCheckBox:
          if not checkbox.isChecked():
            allChecked = False
        self.checkAll.setChecked(allChecked)
    else:
      if self.sender() == self.checkAll:
        for checkbox in self.sectionsCheckBox:
          checkbox.setChecked(False)
      else:
        self.checkAll.setChecked(False)
    self.checkAll.blockSignals(False)
    for checkbox in self.sectionsCheckBox:
      checkbox.blockSignals(False)
  def getData(self):
    selected_sections = []
    for checkbox in self.sectionsCheckBox:
      if checkbox.isChecked():
        selected_sections.append(checkbox.text())
    # print(selected_sections)
    return self.labid, self.labname, selected_sections
