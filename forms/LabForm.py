"""
LabForm.py - LabForm class for MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
from PyQt5.QtWidgets import *

class LabForm(QDialog):
  def __init__(self, id=-1, name="", sectionid=-1):
    super(LabForm, self).__init__()

    self.id = id
    self.name = name
    self.sectionid = sectionid

    self.createFormGroupBox()
    buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
    buttonBox.accepted.connect(self.accept)
    buttonBox.rejected.connect(self.reject)

    mainLayout = QVBoxLayout()
    mainLayout.addWidget(self.formGroupBox)
    mainLayout.addWidget(buttonBox)
    self.setLayout(mainLayout)

    self.setWindowTitle("Lab Form")

  def createFormGroupBox(self):
    self.formGroupBox = QGroupBox("Lab Form")
    self.layout = QFormLayout()
    self.labLineEdit = QLineEdit()
    self.labLineEdit.setText(self.name)
    self.layout.addRow(QLabel("Lab Name:"), self.labLineEdit)
    self.formGroupBox.setLayout(self.layout)

  def getData(self):
    return self.id, self.labLineEdit.text(), self.sectionid
