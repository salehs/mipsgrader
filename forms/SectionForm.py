"""
SectionForm.py - SectionForm class for MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
from PyQt5.QtWidgets import *

class SectionForm(QDialog):
  def __init__(self, id=-1, course="", sectionnum=""):
    super(SectionForm, self).__init__()
    self.id = id
    self.course = course
    self.sectionnum = sectionnum

    self.createFormGroupBox()
    buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
    buttonBox.accepted.connect(self.accept)
    buttonBox.rejected.connect(self.reject)

    mainLayout = QVBoxLayout()
    mainLayout.addWidget(self.formGroupBox)
    mainLayout.addWidget(buttonBox)
    self.setLayout(mainLayout)

    self.setWindowTitle("Section Form")

  def createFormGroupBox(self):
    self.formGroupBox = QGroupBox("Section Form")
    self.layout = QFormLayout()
    self.courseLineEdit = QLineEdit()
    self.courseLineEdit.setText(self.course)
    self.layout.addRow(QLabel("Course:"), self.courseLineEdit)
    self.sectionLineEdit = QLineEdit()
    self.sectionLineEdit.setText(self.sectionnum)
    self.layout.addRow(QLabel("Section Number:"), self.sectionLineEdit)
    self.formGroupBox.setLayout(self.layout)

  def getData(self):
    return self.id, self.courseLineEdit.text(), self.sectionLineEdit.text()
