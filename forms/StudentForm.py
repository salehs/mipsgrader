"""
StudentForm.py - StudentForm class for MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
from PyQt5.QtWidgets import *

class StudentForm(QDialog):
  def __init__(self, sectionid=-1):
    super(StudentForm, self).__init__()
    self.sectionid = sectionid

    self.createFormGroupBox()
    buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
    buttonBox.accepted.connect(self.accept)
    buttonBox.rejected.connect(self.reject)

    mainLayout = QVBoxLayout()
    mainLayout.addWidget(self.formGroupBox)
    mainLayout.addWidget(buttonBox)
    self.setLayout(mainLayout)

    self.setWindowTitle("Student Form")

  def createFormGroupBox(self):
    self.formGroupBox = QGroupBox("Student Form")
    self.layout = QFormLayout()
    self.idLineEdit = QLineEdit()
    self.layout.addRow(QLabel("Student ID:"), self.idLineEdit)
    self.fnameLineEdit = QLineEdit()
    self.layout.addRow(QLabel("First Name:"), self.fnameLineEdit)
    self.lnameLineEdit = QLineEdit()
    self.layout.addRow(QLabel("Last Name:"), self.lnameLineEdit)
    self.formGroupBox.setLayout(self.layout)

  def getData(self):
    return  self.idLineEdit.text(), self.fnameLineEdit.text(), self.lnameLineEdit.text(), self.sectionid
