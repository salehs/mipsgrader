"""
TaskForm.py - TaskForm class for MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
from PyQt5.QtWidgets import *


class TaskForm(QDialog):
  def __init__(self, id=-1, name="", weight=0.0, labid=-1):
    super(TaskForm, self).__init__()

    self.id = id
    self.name = name
    self.weight = weight
    self.labid = labid

    self.createFormGroupBox()
    buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
    buttonBox.accepted.connect(self.accept)
    buttonBox.rejected.connect(self.reject)

    mainLayout = QVBoxLayout()
    mainLayout.addWidget(self.formGroupBox)
    mainLayout.addWidget(buttonBox)
    self.setLayout(mainLayout)

    self.setWindowTitle("Task Form")

  def createFormGroupBox(self):
    self.formGroupBox = QGroupBox("Task Form")
    self.layout = QFormLayout()
    self.taskLineEdit = QLineEdit()
    self.taskLineEdit.setText(self.name)
    self.layout.addRow(QLabel("Task Name:"), self.taskLineEdit)
    self.weightLineEdit = QLineEdit()
    self.weightLineEdit.setText("{}".format(self.weight))
    self.layout.addRow(QLabel("Task Weight:"), self.weightLineEdit)
    self.formGroupBox.setLayout(self.layout)

  def getData(self):
    return self.id, self.labid, self.taskLineEdit.text(), self.weightLineEdit.text()
