"""
TestCaseForm.py - TestCaseForm class for MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
from PyQt5.QtWidgets import *


class TestCaseForm(QDialog):
  def __init__(self, id=-1, name="", arguments="", expected_output="", taskid=-1):
    super(TestCaseForm, self).__init__()

    self.id = id
    self.name = name
    self.arguments = arguments
    self.expected_output = expected_output
    self.taskid = taskid

    self.createFormGroupBox()
    buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
    buttonBox.accepted.connect(self.accept)
    buttonBox.rejected.connect(self.reject)

    mainLayout = QVBoxLayout()
    mainLayout.addWidget(self.formGroupBox)
    mainLayout.addWidget(buttonBox)
    self.setLayout(mainLayout)

    self.setWindowTitle("Test Case Form")

  def createFormGroupBox(self):
    self.formGroupBox = QGroupBox("Test Case Form")
    self.layout = QFormLayout()
    self.testCaseLineEdit = QLineEdit()
    self.testCaseLineEdit.setText(self.name)
    self.layout.addRow(QLabel("Test Case Name:"), self.testCaseLineEdit)
    self.argsLineEdit = QLineEdit()
    self.argsLineEdit.setText(self.arguments)
    self.layout.addRow(QLabel("Arguments (seperated by comma):"), self.argsLineEdit)
    self.expectedOutputTextEdit = QPlainTextEdit()
    self.expectedOutputTextEdit.setPlainText(self.expected_output)
    self.layout.addRow(QLabel("Expected Output (without the input values):"), self.expectedOutputTextEdit)
    self.formGroupBox.setLayout(self.layout)

  def getData(self):
    return self.id, self.testCaseLineEdit.text(), self.argsLineEdit.text(), self.expectedOutputTextEdit.toPlainText(), self.taskid
