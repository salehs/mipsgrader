"""
main.py - main entry point of MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""

import sys
from PyQt5.QtWidgets import QApplication, QStyleFactory, QMessageBox
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from MainWindow import MainWindow

def main():
  """ main method to launch the MIPS Grader GUI Appliction """
  try:
    QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, True) #enable highdpi scaling
    QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps, True)  #use highdpi icons
    QApplication.setStyle(QStyleFactory.create("fusion"))
    font = QFont("Times", 13, QFont.Normal)
    app = QApplication(sys.argv)
    app.setFont(font)
    MainWindow()
    sys.exit(app.exec_())
  except Exception as exception_occured:
    QMessageBox.information(None, "Critical", "Error Message: {}".format(exception_occured), buttons=QMessageBox.Ok)

if __name__ == "__main__":
  main()
