"""
Lab.py - Lab class for MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""

class Lab(object):
  """
  Lab class to hold data from Lab table
  """
  def __init__(self, labid, name, sectionid):
    self.labid = labid
    self.name = name
    self.sectionid = sectionid

  def save(self, db):
    if self.labid == -1:
      db.execute("INSERT INTO lab (name, sectionid) VALUES(\"{}\", {});".format(self.name, self.sectionid))
      self.labid = db.cur.lastrowid
    else:
      db.execute("UPDATE lab SET name=\"{}\", sectionid={} WHERE labid={};".format(self.name, self.sectionid, self.labid))
    db.commit()

  def delete(self, db):
    if self.labid != -1:
      db.execute("DELETE FROM lab WHERE labid={}".format(self.labid))
      db.commit()

  def __str__(self):
    return "{}_{}".format(self.labid, self.name)

  @classmethod
  def getLabs(cls, db):
    result = db.execute("SELECT * FROM lab;")
    labs = []
    for item in result:
      labs.append(Lab(item[0], item[1], item[2]))
    return labs

  @classmethod
  def getLabsOfSection(cls, db, sectionid):
    result = db.execute("SELECT * FROM lab WHERE sectionid={};".format(sectionid))
    labs = []
    for item in result:
      labs.append(Lab(item[0], item[1], item[2]))
    return labs
