"""
Lab_Grade.py - Lab_Grade class for MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
class Lab_Grade(object):
  """
  Lab_Grade class to hold data from Lab_Grade table
  """

  def __init__(self, labid, studentid, grade):
    self.studentid = studentid
    self.labid = labid
    self.grade = grade

  def save(self, db):
    result = db.execute("SELECT * FROM lab_grade WHERE studentid={} AND labid={};".format(self.studentid, self.labid))
    if len(result) == 0:
      db.execute("INSERT INTO lab_grade (studentid, labid, grade) VALUES({}, {}, {});".format(self.studentid, self.labid, self.grade))
    else:
      db.execute("UPDATE lab_grade SET grade={} WHERE studentid={} AND labid={};".format(
          self.grade, self.studentid, self.labid))
    db.commit()

  def delete(self, db):
    if self.studentid != -1 and self.labid != -1:
      db.execute("DELETE FROM lab_grade WHERE studentid={} AND labid={}".format(self.studentid, self.labid))
      db.commit()

  def __str__(self):
    return "{}_{}_{}".format(self.studentid, self.labid, self.grade)

  @classmethod
  def getLab_Grades(cls, db):
    result = db.execute("SELECT * FROM lab_grade;")
    labGrades = []
    for item in result:
      labGrades.append(Lab_Grade(item[0], item[1], item[2]))
    return labGrades

  @classmethod
  def getLab_GradesOfStudent(cls, db, studentid):
    result = db.execute(
        "SELECT * FROM lab_grade WHERE studentid={};".format(studentid))
    labGrades = []
    for item in result:
      labGrades.append(Lab_Grade(item[0], item[1], item[2]))
    return

  @classmethod
  def getLab_GradeOfStudentAndLab(cls, db, studentid, labid):
    result = db.execute(
        "SELECT * FROM lab_grade WHERE studentid={} AND labid={};".format(studentid, labid))
    if result:
      labGrade = Lab_Grade(result[0][0], result[0][1], result[0][2])
      return labGrade
    else:
      return None

  @classmethod
  def getLab_GradesOfLab(cls, db, labid):
    result = db.execute(
        "SELECT * FROM lab_grade WHERE labid={};".format(labid))
    labGrades = []
    for item in result:
      labGrades.append(Lab_Grade(item[0], item[1], item[2]))
    return labGrades
