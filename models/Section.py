"""
Section.py - Section class for MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
class Section(object):
  """
  Section class to hold data from Section table
  """
  def __init__(self, sectionid, course, sectionnum):
    self.sectionid = sectionid
    self.course = course
    self.sectionnum = sectionnum

  def save(self, db):
    if self.sectionid == -1:
      db.execute("INSERT INTO section (course, sectionnum) VALUES(\"{}\", {});".format(self.course, self.sectionnum))
      self.sectionid = db.cur.lastrowid
    else:
      db.execute("UPDATE section SET course=\"{}\", sectionnum={} WHERE sectionid={};".format(self.course, self.sectionnum, self.sectionid))
    db.commit()

  def __str__(self):
    return "{}_{}_{}".format(self.sectionid, self.course, self.sectionnum)

  def delete(self, db):
    if self.sectionid != -1:
      db.execute("DELETE FROM section WHERE sectionid={}".format(self.sectionid))
      db.commit()

  @classmethod
  def getSections(cls, db):
    result = db.execute("SELECT * FROM section;")
    sections = []
    for item in result:
      sections.append(Section(item[0], item[1], item[2]))
    return sections
