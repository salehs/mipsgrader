"""
Student.py - Student class for MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
class Student(object):
  """
  Student class to hold data from Student table
  """
  def __init__(self, studentid, fname, lname, sectionid, exists=False):
    self.studentid = studentid
    self.fname = fname
    self.lname = lname
    self.sectionid = sectionid
    self.exists = exists

  def save(self, db):
    if self.exists:
      db.execute("UPDATE student SET fname=\"{}\", lname=\"{}\", sectionid={} WHERE studentid={};".format(self.fname, self.lname, self.sectionid, self.studentid))
      self.studentid = db.cur.lastrowid
    else:
      db.execute("INSERT INTO student (studentid, fname, lname, sectionid) VALUES({}, \"{}\", \"{}\", {});".format(self.studentid, self.fname, self.lname, self.sectionid))
    db.commit()


  def delete(self, db):
    if self.exists:
      db.execute("DELETE FROM student WHERE studentid={}".format(self.studentid))
      db.commit()

  def __str__(self):
    return "{}_{} {}".format(self.studentid, self.fname, self.lname)

  @classmethod
  def getStudents(cls, db):
    result = db.execute("SELECT * FROM student;")
    students = []
    for item in result:
      students.append(Student(item[0], item[1], item[2], item[3], exists=True))
    return students

  @classmethod
  def getStudentsOfSection(cls, db, sectionid):
    result = db.execute("SELECT * FROM student WHERE sectionid={};".format(sectionid))
    students = []
    for item in result:
      students.append(Student(item[0], item[1], item[2], item[3], exists=True))
    return students
