"""
Task.py - Task class for MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
class Task(object):
  """
  Task class to hold data from Task table
  """
  def __init__(self, taskid, labid, name, weight):
    self.taskid = taskid
    self.labid = labid
    self.name = name
    self.weight = weight

  def save(self, db):
    if self.taskid == -1:
      db.execute("INSERT INTO task (labid, name, weight) VALUES({}, \"{}\", {});".format(self.labid, self.name, self.weight))
      self.taskid = db.cur.lastrowid
    else:
      db.execute("UPDATE task SET labid={}, name=\"{}\", weight={} WHERE taskid={};".format(self.labid, self.name, self.weight, self.taskid))
    db.commit()

  def delete(self, db):
    if self.taskid != -1:
      db.execute("DELETE FROM task WHERE taskid={}".format(self.taskid))
      db.commit()

  def __str__(self):
    return "{}_{}".format(self.taskid, self.name)

  @classmethod
  def getTasks(cls, db):
    result = db.execute("SELECT * FROM task;")
    tasks = []
    for item in result:
      tasks.append(Task(item[0], item[1], item[2], item[3]))
    return tasks

  @classmethod
  def getTasksOfLab(cls, db, labid):
    result = db.execute("SELECT * FROM task WHERE labid = {};".format(labid))
    tasks = []
    for item in result:
      tasks.append(Task(item[0], item[1], item[2], item[3]))
    return tasks
