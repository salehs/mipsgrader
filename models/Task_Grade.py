"""
Task_Grade.py - Task_Grade class for MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
class Task_Grade(object):
  """
  Task_Grade class to hold data from Task_Grade table
  """

  def __init__(self, studentid, taskid, grade):
    self.studentid = studentid
    self.taskid = taskid
    self.grade = grade

  def add(self, db):
    db.execute("INSERT INTO task_grade (studentid, taskid, grade) VALUES({}, {}, {});".format(self.studentid, self.taskid, self.grade))
    db.commit()  
    
  def update(self, db):
    db.execute("UPDATE task_grade SET grade={} WHERE studentid={} AND taskid={};".format(self.grade, self.studentid, self.taskid))
    db.commit()

  def delete(self, db):
    if self.studentid != -1 and self.taskid != -1:
      db.execute("DELETE FROM task_grade WHERE studentid={} AND taskid={}".format(
          self.studentid, self.taskid))
      db.commit()

  def __str__(self):
    return "{}_{}_{}".format(self.studentid, self.taskid, self.grade)

  @classmethod
  def getTask_Grades(cls, db):
    result = db.execute("SELECT * FROM task_grade;")
    task_grades = []
    for item in result:
      task_grades.append(Task_Grade(item[0], item[1], item[2]))
    return task_grades

  @classmethod
  def getTask_GradesOfStudent(cls, db, studentid):
    result = db.execute(
        "SELECT * FROM task_grade WHERE studentid={};".format(studentid))
    task_grades = []
    for item in result:
      task_grades.append(Task_Grade(item[0], item[1], item[2]))
    return task_grades
  
  @classmethod
  def getTask_GradesOfTask(cls, db, taskid):
    result = db.execute(
        "SELECT * FROM task_grade WHERE taskid={};".format(taskid))
    task_grades = []
    for item in result:
      task_grades.append(Task_Grade(item[0], item[1], item[2]))
    return task_grades
