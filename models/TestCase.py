"""
TestCase.py - TestCase class for MIPS Grader Application
MIPSGrader is licensed under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.
Copyright (c) 2021 Saleh AlSaleh
All rights reserved
"""
class TestCase(object):
  """
  TestCase class to hold data from TestCase table
  """

  def __init__(self, testcaseid, name, arguments, expected_output, taskid):
    self.testcaseid = testcaseid
    self.name = name
    self.arguments = arguments
    self.expected_output = expected_output
    self.taskid = taskid

  def save(self, db):
    if self.testcaseid == -1:
      db.execute("INSERT INTO test_case (name, arguments, expected_output, taskid) VALUES(\"{}\", \"{}\", \"{}\", {});".format(self.name, self.arguments, self.expected_output, self.taskid))
      self.testcaseid = db.cur.lastrowid
    else:
      db.execute("UPDATE test_case SET name=\"{}\", arguments=\"{}\", expected_output=\"{}\", taskid={} WHERE testcaseid={};".format(self.name, self.arguments, self.expected_output, self.taskid, self.testcaseid))
    db.commit()

  def delete(self, db):
    if self.testcaseid != -1:
      db.execute("DELETE FROM test_case WHERE testcaseid={}".format(self.testcaseid))
      db.commit()

  def __str__(self):
    return "{}_{}".format(self.testcaseid, self.name)

  @classmethod
  def getTestCases(cls, db):
    result = db.execute("SELECT * FROM test_case;")
    test_cases = []
    for item in result:
      test_cases.append(TestCase(item[0], item[1], item[2], item[3], item[4]))
    return test_cases

  @classmethod
  def getTestCasesOfTask(cls, db, taskid):
    result = db.execute("SELECT * FROM test_case WHERE taskid = {};".format(taskid))
    test_cases = []
    for item in result:
      test_cases.append(TestCase(item[0], item[1], item[2], item[3], item[4]))
    return test_cases
